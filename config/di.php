<?php
declare(strict_types=1);

return [
    \Platform\Framework\Actions\ActionFactory::class => function (\Psr\Container\ContainerInterface $container) {
        /** @var \ifrolikov\dto\Facade $dtoLib */
        $dtoLib = $container->get(\ifrolikov\dto\Facade::class);
        return new \Platform\Framework\Actions\ActionFactory(
            $dtoLib->getArrayPacker(),
            $dtoLib->getDtoBuilder(),
            $container->get(\Platform\Framework\Actions\RequestMaskFactory::class)
        );
    },
    \Platform\Framework\Actions\RequestMaskFactory::class => function () {
        return new \Platform\Framework\Actions\RequestMaskFactory();
    },
    \Platform\Framework\Routes\RouteManager::class => function () {
        return new \Platform\Framework\Routes\RouteManager();
    },
    \ifrolikov\dto\Facade::class => function () {
        return new \ifrolikov\dto\Facade();
    }
];