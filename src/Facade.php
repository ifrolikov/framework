<?php
declare(strict_types=1);

namespace Platform\Framework;

use Platform\Framework\Actions\AbstractAction;
use Platform\Framework\Actions\ActionFactory;
use Platform\Framework\Routes\RouteManager;
use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Container;

final class Facade
{
    private static $instance;

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var FacadeConfig
     */
    private $config;

    public static function getInstance(FacadeConfig $config = null): Facade
    {
        if (empty(self::$instance)) {
            $config = $config ?? new FacadeConfig();
            self::$instance = new Facade($config);
        }
        return self::$instance;
    }

    /**
     * @param AbstractAction[] $actions
     * @param App $app
     */
    public function activateRoutes(array $actions, App $app): void
    {
        /** @var RouteManager $routeManager */
        $routeManager = $this->container->get(RouteManager::class);
        $routeManager->applyRoutes($app, $actions);
    }

    /**
     * @param ExternalActionInterface $externalAction
     * @return AbstractAction
     */
    public function createAction(ExternalActionInterface $externalAction): AbstractAction
    {
        $requestType = $externalAction->getRequestType()->getValue();
        $actionClass = '\\Platform\\Framework\\Actions\\RequestType\\' . ucfirst(strtolower($requestType));
        /** @var ActionFactory $actionFactory */
        $actionFactory = $this->container->get(ActionFactory::class);
        return $actionFactory->create($actionClass, $externalAction, $this->config->isDebug());
    }

    private function __construct(FacadeConfig $config)
    {
        $this->container = $this->initContainer();
        $this->config = $config;
    }

    private function initContainer(): ContainerInterface
    {
        return new Container(require __DIR__ . '/../config/di.php');
    }
}