<?php
declare(strict_types=1);

namespace Platform\Framework;

abstract class Enum
{
    private $value;

    /**
     * @param $name
     * @param $arguments
     * @return Enum
     * @throws \Exception
     */
    public static function __callStatic($name, $arguments)
    {
        if (!defined(static::class . '::' . $name)) {
            throw new \Exception('Enum variant not exists');
        }
        return static::getInstance($name);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $constant
     * @return static
     */
    private function getInstance(string $constant): Enum
    {
        return new static($constant);
    }

    private function __construct(string $value)
    {
        $this->value = $value;
    }
}