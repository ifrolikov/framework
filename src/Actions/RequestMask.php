<?php
declare(strict_types=1);

namespace Platform\Framework\Actions;

use Platform\Framework\Exceptions\AbsentRequiredUriFields;

class RequestMask
{
    /**
     * @var string
     */
    private $openapiCompatibleUriMask;
    /**
     * @var string[]
     */
    private $requiredUriFields;
    /**
     * @var array
     */
    private $fieldTypes;

    public function __construct(string $openapiCompatibleUriMask, array $fieldTypes = [])
    {
        $this->openapiCompatibleUriMask = $openapiCompatibleUriMask;
        $this->requiredUriFields = $this->parseRequiredUriFields($openapiCompatibleUriMask);
        $this->fieldTypes = $fieldTypes;
    }

    /**
     * @param array $uriFields
     * @return string
     * @throws AbsentRequiredUriFields
     */
    public function getUri(array $uriFields): string
    {
        $this->checkRequiredFields($uriFields);
        $uri = $this->applyUriFieldsToMask($uriFields);
        return $uri;
    }

    public function getRegularExpression(): string
    {
        $result = $this->openapiCompatibleUriMask;
        foreach ($this->requiredUriFields as $field) {
            $fieldType = $this->fieldTypes[$field] ?? null;
            switch ($fieldType) {
                case 'int':
                case 'integer':
                    $replaceMask = '([^\d]+)';
                    break;
                case 'string':
                default:
                    $replaceMask = '([^\/]+)';
                    break;
            }
            $result = str_replace('{$' . $field . '}', $replaceMask, $result);
        }
        return $result;
    }

    public function getSlimUriMask(): string
    {
        $result = $this->openapiCompatibleUriMask;
        foreach ($this->requiredUriFields as $field) {
            $fieldType = $this->fieldTypes[$field] ?? null;
            switch ($fieldType) {
                case 'int':
                case 'integer':
                    $replaceMask = '[0-9]+';
                    break;
                case 'string':
                default:
                    $replaceMask = '.+';
                    break;
            }
            $result = str_replace('{$' . $field . '}', '{' . $field . ':' . $replaceMask . '}', $result);
        }
        return $result;
    }

    public function getOpenApiMask(): string
    {
        return $this->openapiCompatibleUriMask;
    }

    /**
     * @param string $openapiCompatibleUriMask
     * @return string[]
     */
    private function parseRequiredUriFields(string $openapiCompatibleUriMask): array
    {
        preg_match_all('~{\$([^}]+)}~', $openapiCompatibleUriMask, $matches);
        return $matches[1];
    }

    /**
     * @param array $uriFields
     * @throws AbsentRequiredUriFields
     */
    private function checkRequiredFields(array $uriFields): void
    {
        $absentFields = array_diff($this->requiredUriFields, $uriFields);
        if (empty($absentFields)) {
            throw new AbsentRequiredUriFields(implode(', ', $absentFields));
        }
    }

    private function applyUriFieldsToMask(array $uriFields): string
    {
        $uri = $this->openapiCompatibleUriMask;
        foreach ($uriFields as $uriField => $value) {
            $uri = str_replace('{$' . $uriField . '}', $value, $uri);
        }
        return $uri;
    }
}