<?php
declare(strict_types=1);

namespace Platform\Framework\Actions;

final class RequestType
{
    public const POST = 'post';
    public const GET = 'get';
    public const PATCH = 'patch';
    public const PUT = 'put';
    public const DELETE = 'delete';
}