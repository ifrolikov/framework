<?php
declare(strict_types=1);

namespace Platform\Framework\Actions\RequestType;

use Platform\Framework\Actions\AbstractAction;
use Platform\Framework\Actions\RequestType;

final class Delete extends AbstractAction
{
    public function getRequestType(): string
    {
        return RequestType::DELETE;
    }
}