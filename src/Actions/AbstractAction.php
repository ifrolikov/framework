<?php
declare(strict_types=1);

namespace Platform\Framework\Actions;

use ifrolikov\dto\Interfaces\ArrayPackerInterface;
use ifrolikov\dto\Interfaces\DtoBuilderInterface;
use Platform\Framework\ExternalActionInterface;
use Psr\Http\Message\RequestInterface;
use Slim\Http\Response;

abstract class AbstractAction
{
    /**
     * @var ArrayPackerInterface
     */
    private $packer;
    /**
     * @var DtoBuilderInterface
     */
    private $unpacker;
    /**
     * @var RequestMask
     */
    private $requestMask;
    /**
     * @var ExternalActionInterface
     */
    private $externalAction;
    /**
     * @var bool
     */
    private $isDebug;

    abstract public function getRequestType(): string;

    public function __construct(
        ArrayPackerInterface $packer,
        DtoBuilderInterface $unpacker,
        RequestMaskFactory $requestMaskFactory,
        ExternalActionInterface $externalAction,
        bool $isDebug
    ) {
        $this->packer = $packer;
        $this->unpacker = $unpacker;
        $this->requestMask = $requestMaskFactory->create(
            $externalAction->getOpenapiCompatibleUriMask(),
            $externalAction->getUriFieldTypes()
        );
        $this->externalAction = $externalAction;
        $this->isDebug = $isDebug;
    }

    /**
     * @param RequestInterface $request
     * @param Response $response
     * @param array $args
     * @throws \Throwable
     */
    public function __invoke(RequestInterface $request, Response $response, array $args): void
    {
        try {
            $dataForRequestDto = $this->externalAction->getDataForRequestDto($request);
            $dataForRequestDto = array_merge($args, $dataForRequestDto);
            try {
                $requestDto = $this->unpacker->setData($dataForRequestDto)->build(
                    $this->externalAction->getRequestDtoClass()
                );
            } catch (\Throwable $exception) {
                $response->withStatus(ResponseStatuses::BAD_REQUEST);
                return;
            }
            $responseDto = $this->externalAction->produceResponseDto($requestDto);
            $dataForResponse = $this->packer->pack($responseDto);

            $response->withStatus(ResponseStatuses::SUCCESS);
            $response->withJson($dataForResponse);
        } catch (\Throwable $exception) {
            if ($this->isDebug) {
                exit((string) $exception);
            }
            throw $exception;
        }
    }

    /**
     * @return RequestMask
     */
    public function getRequestMask(): RequestMask
    {
        return $this->requestMask;
    }

    protected function getPacker(): ArrayPackerInterface
    {
        return $this->packer;
    }

    protected function getUnpacker(): DtoBuilderInterface
    {
        return $this->unpacker;
    }
}