<?php
declare(strict_types=1);

namespace Platform\Framework\Actions;

final class ResponseStatuses
{
    const BAD_REQUEST = 400;
    const SUCCESS = 200;
    const UNPROCESSABLE_ENTITY = 422;
}