<?php
declare(strict_types=1);

namespace Platform\Framework\Actions;

class RequestMaskFactory
{
    public function create(string $openapiCompatibleUriMask, array $fieldTypes = []): RequestMask
    {
        return new RequestMask($openapiCompatibleUriMask, $fieldTypes);
    }
}