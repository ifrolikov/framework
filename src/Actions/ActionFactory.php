<?php
declare(strict_types=1);

namespace Platform\Framework\Actions;

use ifrolikov\dto\Interfaces\ArrayPackerInterface;
use ifrolikov\dto\Interfaces\DtoBuilderInterface;
use Platform\Framework\ExternalActionInterface;

final class ActionFactory
{
    /**
     * @var ArrayPackerInterface
     */
    private $packer;
    /**
     * @var DtoBuilderInterface
     */
    private $unpacker;
    /**
     * @var RequestMaskFactory
     */
    private $requestMaskFactory;

    public function __construct(
        ArrayPackerInterface $packer,
        DtoBuilderInterface $unpacker,
        RequestMaskFactory $requestMaskFactory
    ) {
        $this->packer = $packer;
        $this->unpacker = $unpacker;
        $this->requestMaskFactory = $requestMaskFactory;
    }

    public function create(string $actionClass, ExternalActionInterface $externalAction, bool $isDebug): AbstractAction
    {
        return new $actionClass(
            $this->packer,
            $this->unpacker,
            $this->requestMaskFactory,
            $externalAction,
            $isDebug
        );
    }
}