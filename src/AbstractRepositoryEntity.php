<?php
declare(strict_types=1);

namespace Platform\Framework;

use Doctrine\ORM\Mapping as ORM;

abstract class AbstractRepositoryEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @var int
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $created;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $modified;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): string
    {
        return $this->created;
    }

    public function getModified(): string
    {
        return $this->modified;
    }
}