<?php
declare(strict_types=1);

namespace Platform\Framework;

/**
 * Class RequestType
 * @package Platform\Framework
 * @method RequestType POST() public static
 * @method RequestType GET() public static
 * @method RequestType PATCH() public static
 * @method RequestType PUT() public static
 * @method RequestType DELEtE() public static
 */
final class RequestType extends Enum
{
    public const POST = 'post';
    public const GET = 'get';
    public const PATCH = 'patch';
    public const PUT = 'put';
    public const DELETE = 'delete';
}