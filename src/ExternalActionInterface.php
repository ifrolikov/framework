<?php
declare(strict_types=1);

namespace Platform\Framework;

use Psr\Http\Message\RequestInterface;

interface ExternalActionInterface
{
    public function getRequestType(): RequestType;

    public function getOpenapiCompatibleUriMask(): string;

    /**
     * @return string[]
     */
    public function getUriFieldTypes(): array;

    public function getDataForRequestDto(RequestInterface $request): array;

    public function getRequestDtoClass(): string;

    public function produceResponseDto(object $requestDto): object;
}