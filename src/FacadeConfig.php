<?php
declare(strict_types=1);

namespace Platform\Framework;

class FacadeConfig
{
    private $debug = false;

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     * @return FacadeConfig
     */
    public function setDebug(bool $debug): FacadeConfig
    {
        $this->debug = $debug;
        return $this;
    }
}