<?php
declare(strict_types=1);

namespace Platform\Framework\Routes;

use Platform\Framework\Actions\AbstractAction;
use Platform\Framework\Actions\RequestType;
use Slim\App;

class RouteManager
{
    /**
     * @param App $app
     * @param AbstractAction[] $actions
     */
    public function applyRoutes(App $app, array $actions): void
    {
        foreach ($actions as $action) {
            switch ($action->getRequestType()) {
                case RequestType::GET:
                    $app->get($action->getRequestMask()->getSlimUriMask(), $action);
                    break;
                case RequestType::POST:
                    $app->post($action->getRequestMask()->getSlimUriMask(), $action);
                    break;
                case RequestType::PUT:
                    $app->put($action->getRequestMask()->getSlimUriMask(), $action);
                    break;
                case RequestType::DELETE:
                    $app->delete($action->getRequestMask()->getSlimUriMask(), $action);
                    break;
                case RequestType::PATCH:
                    $app->patch($action->getRequestMask()->getSlimUriMask(), $action);
                    break;
            }
        }
    }
}